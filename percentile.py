import time
import math
import numpy as np
from tabulate import tabulate
from collections import Counter

start = time.time()

log_file = './stats.txt'


def parse_data_from_txt(file_name_txt):
    """gather data"""
    events_dict = {}
    with open(file_name_txt, 'r') as file:
        for line in file:
            key = line.split()[1]
            val = int(line.split()[-1])

            events_dict.setdefault(key, [])
            events_dict[key].append(val)

    return events_dict


def percentile(N, P):
    n = int(math.ceil(P * len(N) + 0.5))
    if n > 1:
        return N[n - 2]
    else:
        return N[0]


### CREATE TABLE

def round_int(number):
    return 5 * ((number + 4) // 5)


def rounde_dict_list(val_list):
    """round list of values"""
    for i in range(len(val_list)):
        val_list[i] = round_int(val_list[i])

    return val_list


def create_dict_statistic_per_request():
    """collect statistics for each request."""

    event_dict_set = {key: sorted(set(event_dict[key])) for key in event_dict}

    statistic_dict = {}

    for key in event_dict_set:
        statistic_dict.setdefault(key, {})

        for val_key in event_dict_set[key]:

            # statistic_dict[].setdefault(round_int(val_key), [])

            if round_int(val_key) in statistic_dict[key]:
                statistic_dict[key][round_int(val_key)] = statistic_dict[key][round_int(val_key)] + event_dict[
                    key].count(val_key)
            else:
                statistic_dict[key][round_int(val_key)] = event_dict[key].count(val_key)

    return statistic_dict


def gen_weight(count_transaction, all_count_by_event):
    """Percentage of total transactions"""
    return count_transaction * (100 / all_count_by_event)


def gen_date_for_table(statistic_dict, event_type):
    """calculate the data for the table for each event"""
    date_tabular = []

    for key in sorted(statistic_dict.keys()):

        if not date_tabular:
            percent = gen_weight(statistic_dict[key], len_req_per_event[event_type])
        else:
            percent = date_tabular[-1][-1] + gen_weight(statistic_dict[key], len_req_per_event[event_type])

        date_tabular.append([key,
                             statistic_dict[key],
                             gen_weight(statistic_dict[key], len_req_per_event[event_type]),
                             percent
                             ])

    return date_tabular


def print_table(statistic_per_event):
    """ print table"""
    for each_evet_type in statistic_per_event:
        print(tabulate(tabular_data=gen_date_for_table(statistic_per_event[each_evet_type], each_evet_type),
                       headers=['ExecTime', 'TransNo', 'Weight,%', 'Percent'], tablefmt='orgtbl'))


def minimum_time_request():
    for key, val in event_dict.items():
        print("Event name: {eventype}, min={min}".format(min=val[0], eventype=key))


# parse txt
event_dict = parse_data_from_txt(log_file)
len_req_per_event = {}

# sorting data
for key in event_dict:
    len_req_per_event[key] = len(event_dict[key])
    event_dict[key].sort()

minimum_time_request()

# create median and percentile by list
for perc in [0.50, 0.90, 0.99, 0.999]:
    for key, val in event_dict.items():
        print("Event name: {eventype}, Percentile {perc} % = {time} microsecond.".format(time=percentile(val, perc),
                                                                                         perc=perc * 100, eventype=key))
# print table
print_table(create_dict_statistic_per_request())

print("\n\nCalculation time: {}".format(time.time() - start))
